#!/usr/bin/env node
'use strict';

const logFile = 'access.log.xz';
const lineReader = require('readline').createInterface({
    input: require('fs').createReadStream(logFile)
});

const moment = require('moment');
const _ = require('lodash');

const FROM_DATE="2017-06-10 00:00:00";
const TO_DATE="2017-09-19 23:59:59";

const fromDate = moment(FROM_DATE, 'YYYY-MM-DD HH:mm:ss');
const toDate = moment(TO_DATE, 'YYYY-MM-DD HH:mm:ss');

const ips = [];
lineReader.on('line', line => {
    // ignored timezone
    const matches = line.match(/^(\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})\s-\s-\s\[(\d{2}\/[A-Za-z]{3}\/\d{4}:\d{2}:\d{2}:\d{2}).*\]\s\"/);
    if (matches && matches[1] && matches[2]) {
        const recordDate = moment(matches[2], 'DD/MMM/YYYY:HH:mm:ss');

        if (recordDate.isAfter(toDate)) {
            // stop reading file
            lineReader.close();
        } else if (recordDate.isSameOrAfter(fromDate)) {
            ips.push({ ip: matches[1] });
        }
    }
});

lineReader.on('close', () => {
    const result = _.chain(ips)
        .groupBy('ip')
        .sortBy(grouped => -grouped.length)
        .slice(0, 10)
        .map(ip => ip[0].ip) // return any element is the same
        .value();

    console.log(`Top 10 hosts makes most requests from ${FROM_DATE} to ${TO_DATE}:`, result);
});
