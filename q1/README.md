### Q1 Scripts

##### Count the total number of HTTP requests recorded by this access logfile
count-total

##### Find the top-10 (host) hosts makes most requests from 2017-06-10 00:00:00 to 2017-06-19 23:59:59, inclusively
find-10-most-hosts.js
  * ```nvm use && npm install```

##### Find out the country with most requests originating from (according the source IP)
find-most-requests-country.js
  * ```nvm use && npm install```
  * ```docker-compose up -d freegeoip``` for country ip lookup