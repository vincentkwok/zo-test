#!/usr/bin/env node

'use strict';

const FREEGEOIP_HOST = 'freegeoip.localhost:8080';

const logFile = 'access.log.xz';
const lineReader = require('readline').createInterface({
    input: require('fs').createReadStream(logFile)
});

const childProcess = require('child_process');

const ips = [];
lineReader.on('line', line => {
    ips.push(line.split(' ').shift());
});

lineReader.on('close', () => {
    const countByCountry = {};
    const ipGeoMapping = {};

    // blocking curl to avoid request rate limit
    ips.forEach(ip => {
        if (typeof ipGeoMapping[ip] === 'undefined') {
            const cmd = `curl -s ${FREEGEOIP_HOST}/json/${ip}`;
            const option = { encoding: 'UTF8' };
            const curlResult = JSON.parse(childProcess.execSync(cmd, option));

            // TODO: error handling ?!
            ipGeoMapping[ip] = curlResult.country_name;
            countByCountry[ipGeoMapping[ip]] = 0;
        }

        countByCountry[ipGeoMapping[ip]] += 1;
    });

    let mostCount = 0;
    let mostCountCountry = '';
    Object.keys(countByCountry).forEach(countryName => {
        if (countByCountry[countryName] > mostCount) {
            mostCount = countByCountry[countryName];
            mostCountCountry = countryName;
        }
    });

    console.log(`Most requests originating from: ${mostCountCountry}`);
});
