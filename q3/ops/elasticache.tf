resource "aws_elasticache_cluster" "bitly_redis" {
  cluster_id           = "bitly-redis"
  engine               = "redis"
  engine_version       = "3.2.10"
  node_type            = "cache.t2.micro"
  port                 = 6379
  num_cache_nodes      = 1
  /* TODO: Multiaz, Read replica */
}