resource "aws_ecr_repository" "app" {
    name = "app"
}

resource "aws_ecs_cluster" "app_cluster" {
    name = "app-ecs-cluster"
}

data "template_file" "ecs_task" {
    template = "${file("ecs_tasks/task.json")}"
}

resource "aws_ecs_task_definition" "app" {
    family = "app"
    container_definitions = "${data.template_file.ecs_task.rendered}"
}

resource "aws_ecs_service" "app" {
    name = "app"

    cluster = "${aws_ecs_cluster.app_cluster.id}"
    task_definition = "${aws_ecs_task_definition.app.arn}"

    desired_count = 2
    deployment_minimum_healthy_percent = 50

    load_balancer {
        target_group_arn = "${aws_alb.app_alb.arn}"
        container_name = "app"
        container_port = 3000
    }

    lifecycle {
        ignore_changes = ["task_definition"]
    }
}