resource "aws_alb" "app_alb" {
    name = "app-alb"
    internal = false
    security_groups = ["${var.default_sg}", "${aws_security_group.web.id}"]
    subnets = ["${aws_subnet.public1.id}", "${aws_subnet.public2.id}"]
    enable_deletion_protection = true
    idle_timeout = 300
}

resource "aws_alb_listener" "alb_http_listener" {
    load_balancer_arn = "${aws_alb.app_alb.arn}"
    port = "80"
    protocol = "HTTP"
    default_action {
        target_group_arn = "${aws_alb_target_group.default.arn}"
        type = "forward"
    }
}

resource "aws_alb_target_group" "default" {
    name = "default-target-group"
    port = 3000
    protocol = "HTTP"
    vpc_id = "${aws_vpc.main.id}"

    health_check {
        interval = 30
        /* TODO: create endpoint for ping */
        path = "/"
        port = "3000"
        protocol = "HTTP"
        timeout = 5
        healthy_threshold = 5
        unhealthy_threshold = 2
        matcher = "200"
    }
}