variable "ecs_ami_id" {
    default = "ami-1bdc8b78"
}

variable "demo_key" {
    default = "demo"
}

data "template_file" "ecs_user_data" {
    template = "${file("user_data/ecs.sh")}"

    vars {
        cluster_name = "${aws_ecs_cluster.app_cluster.name}"
    }
}

resource "aws_launch_configuration" "ecs" {
    name_prefix = "ecs-"

    image_id = "${var.ecs_ami_id}"
    instance_type = "t2.small"
    key_name = "${var.demo_key}"

    user_data = "${data.template_file.ecs_user_data.rendered}"

    root_block_device {
        volume_type = "gp2"
        volume_size = "8"
    }

    ebs_block_device {
        device_name = "/dev/xvdcz"
        volume_type = "gp2"
        volume_size = "100"
    }

    security_groups = [
        "${var.default_sg}"
    ]

    lifecycle {
        create_before_destroy = true
    }
}

resource "aws_autoscaling_group" "ecs" {
    name = "ecs"
    /* TODO: should create private subnet for it */
    vpc_zone_identifier = ["${aws_subnet.public1.id}", "${aws_subnet.public2.id}"]
    launch_configuration = "${aws_launch_configuration.ecs.name}"
    target_group_arns = ["${aws_alb_target_group.default.arn}"]

    desired_capacity = 2
    min_size = 1
    max_size = 2

    health_check_grace_period = 300
    health_check_type = "EC2"
    force_delete = true

    tag {
        key = "Name"
        value = "[ECS] Container Instance"
        propagate_at_launch = true
    }

    tag {
        key = "Role"
        value = "ecs"
        propagate_at_launch = true
    }

    lifecycle {
        create_before_destroy = true
    }
}
