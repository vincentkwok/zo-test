'use strict';

const express = require('express');
const bodyParser = require('body-parser')

const app = express();

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

const routing = require('./libs/routing');

// TODO: check shortenLink constraint
app.get('/:shortenLink', routing.getShortenLink);
app.post('/submit', routing.createShortenLink);

app.listen(3000, () => console.log('Example app listening on port 3000!'));
