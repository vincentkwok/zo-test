const env = process.env.NODE_ENV || 'dev';

module.exports = {
    dev: {
        redis_host: 'dev.docker'
    },
    production: {
        redis_host: 'bitly-redis.amt9ny.0001.apse1.cache.amazonaws.com'
    }
}[env];