'use strict';

const redisClient = require('./redis');
const randomString = require('randomstring');

module.exports = {
    getShortenLink: (req, res) => {
        // TODO: handle not found
        redisClient.getAsync(req.params.shortenLink).then(result => {
            res.redirect(301, result);
        });
    },
    createShortenLink: (req, res, next) => {
        getSetKeyPromise(req.body.url)
            .then(result => {
                res.json({
                    url: req.body.url,
                    shorten_url: result
                });
            })
            .catch(err => {
                res.json(err);
            });
    }
};

function getSetKeyPromise(url) {
    const shortenLink = randomString.generate({
        length: 8,
        charset: 'alphabetic'
    });

    return redisClient.setAsync(shortenLink, url, 'NX').then(result => {
        if (result !== 'OK') {
            return getSetKeyPromise(url);
        }
        return shortenLink;
    });
}

