'use strict';

const config = require('./config');

const redis = require('redis');
const bluebird = require('bluebird');
bluebird.promisifyAll(redis.RedisClient.prototype);

const client = redis.createClient({
    host: config.redis_host,
});

client.on('connect', () => console.log('connected redis'));

module.exports = client;