#!/bin/bash

nvm use && npm install

docker build -t app:latest .
# TODO: do not commit

aws ecr get-login --region ap-southeast-1 | bash

docker tag app:latest 370241353864.dkr.ecr.ap-southeast-1.amazonaws.com/app:latest
docker push 370241353864.dkr.ecr.ap-southeast-1.amazonaws.com/app:latest